# drp-webhooks

testing DRP webhooks more 

## Getting started

This content pack contains examples used by Rob Hirschfeld for demos.

## Installing the Webhook Trigger & Content

You can automatically install this content pack by using the following trigger. Once installed using the trigger, it will keep itself updated.

```
    Name: zehicle-content-updater
    Description: 'update content automatically'
    Blueprint: dr-server-add-content-from-git
    Enabled: true
    AllInFilter: true
    FilterCount: 1
    Filter: Params.machine-self-runner=true Endpoint=
    MergeDataIntoParams: true
    TriggerProvider: git-lab-trigger-webhook-push
    Params:
      git-lab-trigger/secret: provide_me
      git-lab-trigger/webhook-push/repo_url: https://gitlab.com/zehicle/drp-webhooks
    WorkOrderParams:
      dr-server/update-content: true
      manager/update-catalog: false
    Meta:
      color: black
      icon: lab
```

## Create Webhook from Gitlab

You must also create a matching webhook to push to the Digital Rebar instance.

- URL: https://[drpip]:8092/webhooks/git-lab-trigger-webhook-push
- Secret token must match `git-lab-trigger/secret`
- push events
- DISABLE SSL verification
